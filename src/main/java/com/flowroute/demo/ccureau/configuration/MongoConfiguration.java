package com.flowroute.demo.ccureau.configuration;

import io.micronaut.context.annotation.ConfigurationProperties;
import io.micronaut.core.annotation.NonNull;
import javax.validation.constraints.NotNull;

@ConfigurationProperties("docdb")
public class MongoConfiguration implements MongoConf {
    @NonNull
    @NotNull
    private String url;

    @NonNull
    @NotNull
    private String databaseName;

    @NonNull
    @NotNull
    private String username;

    @NonNull
    @NotNull
    private String password;

    @NonNull
    @NotNull
    private String cacheCollectionName;

    @NonNull
    @NotNull
    private String listCollectionName;

    @NonNull
    @NotNull
    private String overrideCollectionName;

    @NonNull
    @NotNull
    private String cacheExpireTime;

    public void setUrl(@NonNull String url) {
        this.url = url;
    }

    public void setDatabaseName(@NonNull String databaseName) {
        this.databaseName = databaseName;
    }

    public void setUsername(@NonNull String username) {
        this.username = username;
    }

    public void setPassword(@NonNull String password) {
        this.password = password;
    }

    public void setCacheCollectionName(@NonNull String cacheCollectionName) {
        this.cacheCollectionName = cacheCollectionName;
    }

    public void setListCollectionName(@NonNull String listCollectionName) {
        this.listCollectionName = listCollectionName;
    }

    public void setOverrideCollectionName(@NonNull String overrideCollectionName) {
        this.overrideCollectionName = overrideCollectionName;
    }

    public void setCacheExpireTime(@NonNull String cacheExpireTime) {
        this.cacheExpireTime = cacheExpireTime;
    }

    @Override
    public String getUrl() {
        return url;
    }

    @Override
    public String getDatabaseName() {
        return databaseName;
    }

    @Override
    public String getUsername() { return username; }

    @Override
    public String getPassword() { return password; }

    @Override
    public String getCacheCollectionName() {
        return cacheCollectionName;
    }

    @Override
    public String getListCollectionName() {
        return listCollectionName;
    }

    @Override
    public String getOverrideCollectionName() {
        return overrideCollectionName;
    }

    @Override
    public String getCacheExpireTime() {
        return cacheExpireTime;
    }
}
