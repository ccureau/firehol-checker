package com.flowroute.demo.ccureau.configuration;

import com.mongodb.client.*;
import com.mongodb.client.model.IndexOptions;
import com.mongodb.client.model.Indexes;
import io.micronaut.context.event.ApplicationEventListener;
import io.micronaut.runtime.event.ApplicationStartupEvent;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Singleton;
import java.util.concurrent.TimeUnit;

@Singleton
public class StartupSequence implements ApplicationEventListener<ApplicationStartupEvent> {

    private final MongoConf mongoConf;
    private final MongoClient mongoClient;
    private final Logger log = LoggerFactory.getLogger(StartupSequence.class);

    public StartupSequence(MongoClient mongoClient, MongoConf mongoConf) {
        this.mongoClient = mongoClient;
        this.mongoConf = mongoConf;
    }

    @Override
    public void onApplicationEvent(ApplicationStartupEvent event) {
        MongoDatabase db = mongoClient.getDatabase(mongoConf.getDatabaseName());
        try {
            // Create collections if they don't already exist

            if (! collectionExists(mongoConf.getDatabaseName(), mongoConf.getListCollectionName())) {
                db = mongoClient.getDatabase(mongoConf.getDatabaseName());
                db.createCollection(mongoConf.getListCollectionName());

                // Create indexes on the list collection
                MongoCollection<Document> collection = db.getCollection(mongoConf.getListCollectionName());
                collection.createIndex(Indexes.ascending("ipAddress"));
                collection.createIndex(Indexes.ascending("firstOctet"));
//                collection.createIndex(
//                        Indexes.compoundIndex(
//                                Indexes.ascending("ipAddress"),
//                                Indexes.ascending("bitMask")
//                        ));

            }

            if (! collectionExists(mongoConf.getDatabaseName(), mongoConf.getCacheCollectionName())) {
                db.createCollection(mongoConf.getCacheCollectionName());

                // Create index and expiration on cache collection
                MongoCollection<Document> collection = db.getCollection(mongoConf.getCacheCollectionName());
                collection.createIndex(Indexes.ascending("createdAt"),
                        new IndexOptions().expireAfter(Long.parseLong(mongoConf.getCacheExpireTime()),
                                TimeUnit.SECONDS));
            }

            if (! collectionExists(mongoConf.getDatabaseName(), mongoConf.getOverrideCollectionName())) {
                db.createCollection(mongoConf.getOverrideCollectionName());

                // Create indexes on override collection
                MongoCollection<Document> collection = db.getCollection(mongoConf.getOverrideCollectionName());
                collection.createIndex(Indexes.ascending("ipAddress"));
                collection.createIndex(Indexes.ascending("status"));
            }

            if (! collectionExists(mongoConf.getDatabaseName(), "admin")) {
                db.createCollection("admin");
            }

        } catch (Exception e) {
            log.info(e.toString());
        }
    }

    private boolean collectionExists(String dbName, String collection) {
        MongoIterable<String> collections = mongoClient.getDatabase(dbName).listCollectionNames();
        MongoCursor<String> iterator = collections.iterator();
        while (iterator.hasNext()) {
            if (iterator.next().equalsIgnoreCase(collection)) {
                return true;
            }
        }
        return false;
    }
}
