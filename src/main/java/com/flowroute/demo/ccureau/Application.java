package com.flowroute.demo.ccureau;

import io.micronaut.runtime.Micronaut;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;

@OpenAPIDefinition(
        info = @Info(
                title = "firehol-checker",
                version = "1.0",
                description = "Fluentstream Firehol Checker",
                license = @License(name = "BSD-3-clause"),
                contact = @Contact(url = "https://gitlab.com/ccureau/firehol-checker", name = "Chris Cureau", email = "cmcureau@gmail.com")
        )
)
public class Application {
    public static void main(String[] args) {
        Micronaut.run(Application.class, args);
    }
}
