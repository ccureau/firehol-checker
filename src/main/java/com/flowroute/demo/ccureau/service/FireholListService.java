package com.flowroute.demo.ccureau.service;

import com.flowroute.demo.ccureau.configuration.MongoConf;
import com.flowroute.demo.ccureau.model.FireholList;
import com.mongodb.BasicDBObject;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import io.micronaut.scheduling.TaskExecutors;
import io.micronaut.scheduling.annotation.ExecuteOn;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Singleton;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;

@Singleton
@ExecuteOn(TaskExecutors.IO)
public class FireholListService {

    private final MongoConf mongoConf;
    private final MongoClient mongoClient;
    private final Logger log = LoggerFactory.getLogger(FireholListService.class);

    public FireholListService(MongoClient mongoClient, MongoConf mongoConf) {
        this.mongoClient = mongoClient;
        this.mongoConf = mongoConf;
    }

    /**
     * save a record into the collection
     */
    public void save(@Valid FireholList entry) {
        getCollection().insertOne(entry);
    }

    /**
     * save all records into the collection
     */
    public void saveAll(List<FireholList> entries) {
        getCollection().insertMany(entries);
    }

    /**
     * Get all documents in the collection
     */
    public List<FireholList> getAllFromList(String listName) {
        List<FireholList> result = new ArrayList<>();
        MongoCollection<FireholList> collection = getCollection();
        FindIterable<FireholList> iterable = collection.find(eq("listName", listName));
        Iterable<FireholList> list = StreamSupport.stream(iterable.spliterator(), false)
                .collect(Collectors.toList());
        list.forEach(result::add);
        return result;
    }

    /**
     * Get one item from the collection
     * Return null if not found
     */
    public FireholList get(String listName, String ipAddress, String maskBits) {
        MongoCollection<FireholList> collection = getCollection();
        FindIterable<FireholList> iterable = collection.find(and(
                eq("listName", listName),
                eq("ipAddress", ipAddress),
                eq("maskBits", maskBits)));
        Iterable<FireholList> list = StreamSupport.stream(iterable.spliterator(), false)
                .collect(Collectors.toList());
        if (list.iterator().hasNext()) {
            return list.iterator().next();
        }
        return null;
    }

    /**
     * Return a list of all documents in the collection that match ipAddress and maskBits
     */
    public List<FireholList> get(String ipAddress, String maskBits) {
        MongoCollection<FireholList> collection = getCollection();
        FindIterable<FireholList> iterable = collection.find(
                and(
                    eq("ipAddress", ipAddress),
                    eq("maskBits", maskBits)
                )
        );
        return StreamSupport.stream(iterable.spliterator(), false)
                .collect(Collectors.toList());
    }

    public List<FireholList> getByIPAddressRegex(String ipAddressRegex) {
        BasicDBObject regex = new BasicDBObject();
        regex.put("ipAddress",
                new BasicDBObject("$regex", ipAddressRegex));
        MongoCollection<FireholList> collection = getCollection();
        FindIterable<FireholList> iterable = collection.find(regex);
        return StreamSupport.stream(iterable.spliterator(), false)
                .collect(Collectors.toList());
    }

    public List<FireholList> getByFirstOctet(String octet) {
        MongoCollection<FireholList> collection = getCollection();
        FindIterable<FireholList> iterable = collection.find(
                eq("firstOctet", octet)
        );
        return StreamSupport.stream(iterable.spliterator(), false)
                .collect(Collectors.toList());
    }

    /**
     * delete a set of documents from the collection
     */
    public void deleteAllFromList(String list) {
        getCollection().deleteMany(eq("listName", list));
    }

    /**
     * find a record in the collection
     */

    public boolean exists(String listName, String ipAddress, String maskBits) {
        MongoCollection<FireholList> c = getCollection();

        try (MongoCursor<FireholList> list = c.find(and(
                eq("listName", listName),
                eq("ipAddress", ipAddress),
                eq("maskBits", maskBits)
        )).iterator()) {
            if (list.hasNext()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Update the collection (delete and replace)
     * TODO: figure out proper updates that don't take aeons to complete
     */
    public void updateCollection(List<FireholList> newEntries, String listName) {
        deleteAllFromList(listName);
        saveAll(newEntries);
    }

    private MongoCollection<FireholList> getCollection() {
        return mongoClient
                .getDatabase(mongoConf.getDatabaseName())
                .getCollection(mongoConf.getListCollectionName(), FireholList.class);
    }
}
