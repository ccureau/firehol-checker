package com.flowroute.demo.ccureau.service;

import com.flowroute.demo.ccureau.configuration.MongoConf;
import com.flowroute.demo.ccureau.model.FireholCache;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import io.micronaut.scheduling.TaskExecutors;
import io.micronaut.scheduling.annotation.ExecuteOn;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Singleton;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static com.mongodb.client.model.Filters.eq;

@Singleton
@ExecuteOn(TaskExecutors.IO)
public class FireholCacheService {

    private final MongoClient client;
    private final MongoConf mongoConf;
    private final Logger log = LoggerFactory.getLogger(FireholCacheService.class);

    public FireholCacheService(MongoClient client, MongoConf mongoConf) {
        this.client = client;
        this.mongoConf = mongoConf;
    }

    /**
     * Find exact IP address in cache, or null if not in cache
     */
    public FireholCache get(String ipAddress) {
        MongoCollection<FireholCache> collection = getCollection();
        FindIterable<FireholCache> iterable = collection.find(eq("ipAddress", ipAddress));
        Iterable<FireholCache> list = StreamSupport.stream(iterable.spliterator(), false)
                .collect(Collectors.toList());
        if (list.iterator().hasNext()) {
            return list.iterator().next();
        }
        return null;
    }

    /**
     * Save IP address record to list
     */
    public void save(FireholCache document) {
        getCollection().insertOne(document);
    }

    private MongoCollection<FireholCache> getCollection() {
        return client
                .getDatabase(mongoConf.getDatabaseName())
                .getCollection(mongoConf.getCacheCollectionName(), FireholCache.class);
    }
}
