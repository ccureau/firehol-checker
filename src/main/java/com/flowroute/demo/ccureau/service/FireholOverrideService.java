package com.flowroute.demo.ccureau.service;

import com.flowroute.demo.ccureau.configuration.MongoConf;
import com.flowroute.demo.ccureau.model.FireholOverride;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import io.micronaut.scheduling.TaskExecutors;
import io.micronaut.scheduling.annotation.ExecuteOn;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Singleton;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static com.mongodb.client.model.Filters.eq;

@Singleton
@ExecuteOn(TaskExecutors.IO)
public class FireholOverrideService {

    private final MongoClient mongoClient;
    private final MongoConf mongoConf;
    private final Logger log = LoggerFactory.getLogger(FireholOverrideService.class);

    public FireholOverrideService(MongoClient mongoClient, MongoConf mongoConf) {
        this.mongoClient = mongoClient;
        this.mongoConf = mongoConf;
    }

    /**
     * Find exact IP address in cache, or null if not in cache
     */
    public FireholOverride get(String ipAddress) {
        MongoCollection<FireholOverride> collection = getCollection();
        FindIterable<FireholOverride> iterable = collection.find(eq("ipAddress", ipAddress));
        Iterable<FireholOverride> list = StreamSupport.stream(iterable.spliterator(), false)
                .collect(Collectors.toList());
        if (list.iterator().hasNext()) {
            return list.iterator().next();
        }
        return null;
    }

    /**
     * Save IP address record to list
     */
    public void save(FireholOverride document) {
        getCollection().insertOne(document);
    }

    /**
     * Remove IP address from list
     */

    public void delete(String ipAddress) {
        getCollection().deleteOne(eq("ipAddress", ipAddress));
    }

    private MongoCollection<FireholOverride> getCollection() {
        return mongoClient
                .getDatabase(mongoConf.getDatabaseName())
                .getCollection(mongoConf.getOverrideCollectionName(), FireholOverride.class);
    }
}
