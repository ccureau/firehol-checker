package com.flowroute.demo.ccureau.factory;

import com.flowroute.demo.ccureau.configuration.MongoConf;
import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.ReadConcern;
import com.mongodb.WriteConcern;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import io.micronaut.context.annotation.Bean;
import io.micronaut.context.annotation.Factory;
import io.micronaut.context.annotation.Replaces;
import org.bson.codecs.configuration.CodecRegistries;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Singleton;

@Factory
public class MongoClientFactory {

    private final MongoConf mongoConf;
    private final Logger log = LoggerFactory.getLogger(MongoClientFactory.class);

    public MongoClientFactory(MongoConf mongoConf) {
        this.mongoConf = mongoConf;
    }

    @Singleton
    @Bean(preDestroy = "close")
    @Replaces(MongoClient.class)
    public MongoClient mongoClient() {
        MongoClient client = null;

        try {
            ConnectionString connectionString = new ConnectionString(
                    "mongodb://" +
                    mongoConf.getUsername() + ":" + mongoConf.getPassword() +
                    "@" + mongoConf.getUrl() +
                    ":27017/?replicaSet=rs0&readPreference=secondaryPreferred&retryWrites=false");
            CodecRegistry pojoRegistry = CodecRegistries.fromProviders(PojoCodecProvider.builder().automatic(true).build());
            CodecRegistry codecRegistry = CodecRegistries.fromRegistries(MongoClientSettings.getDefaultCodecRegistry(), pojoRegistry);
            MongoClientSettings clientSettings = MongoClientSettings.builder()
                    .applyConnectionString(connectionString)
                    .codecRegistry(codecRegistry)
                    .retryWrites(false)
                    .writeConcern(WriteConcern.MAJORITY)
                    .readConcern(ReadConcern.MAJORITY)
                    .build();
            client = MongoClients.create(clientSettings);
        } catch (Exception e) {
            log.error("Error occured", e);
        }
        return client;
    }
}
