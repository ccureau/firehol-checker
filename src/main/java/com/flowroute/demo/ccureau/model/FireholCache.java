package com.flowroute.demo.ccureau.model;

import io.micronaut.core.annotation.Introspected;
import io.micronaut.core.annotation.NonNull;
import io.swagger.v3.oas.annotations.media.Schema;
import org.bson.codecs.pojo.annotations.BsonProperty;
import org.bson.types.ObjectId;

import java.util.Date;
import java.util.List;

@Introspected
@Schema(name = "FireholCache", description = "Firehol Cached Record")
public class FireholCache {
    private ObjectId id;

    @NonNull
    @BsonProperty(value = "ipAddress")
    @Schema(description = "IP address")
    private String ipAddress;

    @NonNull
    @BsonProperty(value = "inLists")
    @Schema(description = "List of files that this IP address appears in")
    private List<String> inLists;

    @NonNull
    @BsonProperty(value = "createdAt")
    @Schema(description = "record creation time")
    private Date createdAt;

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    @NonNull
    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(@NonNull String ipAddress) {
        this.ipAddress = ipAddress;
    }

    @NonNull
    public List<String> getInLists() {
        return inLists;
    }

    public void setInLists(@NonNull List<String> inLists) {
        this.inLists = inLists;
    }

    @NonNull
    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(@NonNull Date createdAt) {
        this.createdAt = createdAt;
    }
}
