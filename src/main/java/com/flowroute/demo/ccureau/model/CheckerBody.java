package com.flowroute.demo.ccureau.model;

import io.micronaut.core.annotation.Introspected;
import io.swagger.v3.oas.annotations.media.Schema;

@Introspected
@Schema(name = "POST Body", description = "body sent to the Checker API")
public class CheckerBody {

    @Schema(description = "IP Address to search")
    private String ipAddress;

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }
}
