package com.flowroute.demo.ccureau.model;

import io.micronaut.core.annotation.Introspected;
import io.micronaut.core.annotation.NonNull;
import org.bson.codecs.pojo.annotations.BsonProperty;
import org.bson.types.ObjectId;

import java.util.Objects;

@Introspected
public class FireholList {

    private ObjectId id;

    @NonNull
    @BsonProperty(value = "ipAddress")
    private String ipAddress;

    @NonNull
    @BsonProperty(value = "firstOctet")
    private String firstOctet;

    @NonNull
    @BsonProperty(value = "maskBits")
    private String maskBits;

    @NonNull
    @BsonProperty(value = "listName")
    private String listName;

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    @NonNull
    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(@NonNull String ipAddress) {
        this.ipAddress = ipAddress;
    }

    @NonNull
    public String getFirstOctet() {
        return firstOctet;
    }

    public void setFirstOctet(@NonNull String firstOctet) {
        this.firstOctet = firstOctet;
    }

    @NonNull
    public String getMaskBits() {
        return maskBits;
    }

    public void setMaskBits(@NonNull String maskBits) {
        this.maskBits = maskBits;
    }

    @NonNull
    public String getListName() {
        return listName;
    }

    public void setListName(@NonNull String listName) {
        this.listName = listName;
    }

    public boolean equals(FireholList item) {
        return this.listName.equals(item.getListName())
                && this.ipAddress.equals(item.getIpAddress())
                && this.firstOctet.equals(item.getFirstOctet())
                && this.maskBits.equals(item.getMaskBits());
    }

    public int hash() {
        return Objects.hash(this.ipAddress, this.firstOctet, this.maskBits, this.listName);
    }
}
