package com.flowroute.demo.ccureau.model;

import io.micronaut.core.annotation.Introspected;
import io.micronaut.core.annotation.NonNull;
import org.bson.codecs.pojo.annotations.BsonProperty;

@Introspected

public class FireholOverride {

    public enum Status {
        ALLOW, DENY
    }

    @NonNull
    @BsonProperty(value = "ipAddress")
    private String ipAddress;

    @NonNull
    @BsonProperty(value = "status")
    private Status status;

    @NonNull
    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(@NonNull String ipAddress) {
        this.ipAddress = ipAddress;
    }

    @NonNull
    public Status getStatus() {
        return status;
    }

    public void setStatus(@NonNull Status status) {
        this.status = status;
    }
}
