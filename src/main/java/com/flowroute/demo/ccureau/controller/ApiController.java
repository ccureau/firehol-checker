package com.flowroute.demo.ccureau.controller;

import com.flowroute.demo.ccureau.model.CheckerBody;
import com.flowroute.demo.ccureau.model.FireholCache;
import com.flowroute.demo.ccureau.model.FireholList;
import com.flowroute.demo.ccureau.model.FireholOverride;
import com.flowroute.demo.ccureau.service.FireholCacheService;
import com.flowroute.demo.ccureau.service.FireholListService;
import com.flowroute.demo.ccureau.service.FireholOverrideService;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Post;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.apache.commons.net.util.SubnetUtils;

import java.util.*;

@Controller("/api")
public class ApiController {

    private final FireholListService fireholListService;
    private final FireholCacheService fireholCacheService;
    private final FireholOverrideService fireholOverrideService;

    public ApiController(FireholListService fireholListService,
                         FireholCacheService fireholCacheService, FireholOverrideService fireholOverrideService) {
        this.fireholListService = fireholListService;
        this.fireholCacheService = fireholCacheService;
        this.fireholOverrideService = fireholOverrideService;
    }

    @Post(value = "/check", consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
    @Operation(summary = "Check a specific IP address",
        description = "Queries the Firehol lists for the provided IP address, returning each list that the address appears in")
    @Parameter(name = "ipAddress", description = "The IP address to search for")
    @ApiResponse(responseCode = "200", description = "check succesful")
    @ApiResponse(responseCode = "400", description = "ipAddress missing from body")
    @ApiResponse(responseCode = "400", description = "Invalid IP address provided")
    public HttpResponse<?> check(@Body CheckerBody args) {
        String ipAddress = args.getIpAddress();
        if (ipAddress == null) {
            return HttpResponse.badRequest("Missing argument: ipAddress");
        }

        try {
            SubnetUtils su = new SubnetUtils(ipAddress + "/32");
        } catch (Exception e) {
            return HttpResponse.badRequest("ipAddress is invalid");
        }

        // First, check the overrides
        FireholOverride override = fireholOverrideService.get(ipAddress);
        if (override != null) {
            FireholCache cache = new FireholCache();
            cache.setIpAddress(override.getIpAddress());
            List<String> inLists = new ArrayList<>();
            if (override.getStatus().equals(FireholOverride.Status.ALLOW)) {
                inLists.add(FireholOverride.Status.ALLOW.toString());
            } else {
                inLists.add(FireholOverride.Status.DENY.toString());
            }
            cache.setInLists(inLists);
            return HttpResponse.ok(generateResponse(cache));
        }

        // If we find the IP in the cache, we're done
        FireholCache cached = fireholCacheService.get(ipAddress);
        if (cached != null) {
            return HttpResponse.ok(generateResponse(cached));
        }

        // else, look for exact matches (/32)
        cached = new FireholCache();
        cached.setIpAddress(ipAddress);
        cached.setCreatedAt(new Date());

        List<String> cacheList = new ArrayList<>();
        List<FireholList> matches = fireholListService.get(ipAddress, "32");
        for (FireholList fl : matches) {
            if (! cacheList.contains(fl.getListName())) {
                cacheList.add(fl.getListName());
            }
        }

        // next, subnets that are close (!/32)
        String[] tokens = ipAddress.split("\\.");
        matches = fireholListService.getByFirstOctet(tokens[0]);
        // matches = fireholListService.getByIPAddressRegex("^" + tokens[0] + "\\..*");
        for (FireholList fl : matches) {
            if (! fl.getMaskBits().equals("32")) {
                SubnetUtils su = new SubnetUtils(fl.getIpAddress() + "/" + fl.getMaskBits());
                if (su.getInfo().isInRange(ipAddress)) {
                    if (! cacheList.contains(fl.getListName())) {
                        cacheList.add(fl.getListName());
                    }
                }
            }
        }
        cached.setInLists(cacheList);
        fireholCacheService.save(cached);
        return HttpResponse.ok(generateResponse(cached));
    }

    private Map<String, Object> generateResponse(FireholCache cache) {
        Map<String, Object> result = new HashMap<>();
        result.put("ipAddress", cache.getIpAddress());
        if (cache.getInLists().isEmpty()) {
            List<String> none = new ArrayList<>();
            none.add("none");
            result.put("inLists", none);
        } else {
            result.put("inLists", cache.getInLists());
        }
        result.put("creationTime", cache.getCreatedAt());
        return result;
    }

    @Post(value = "/exception/allow", consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
    @Operation(summary = "Add an ALLOW exception for a specific IP address",
            description = "Add an ALLOW exception for a specific IP address, if it does not already exist")
    @Parameter(name = "ipAddress", description = "The IP address to search for")
    @ApiResponse(responseCode = "200", description = "ALLOW successful")
    @ApiResponse(responseCode = "400", description = "ipAddress missing from body")
    @ApiResponse(responseCode = "400", description = "Invalid IP address provided")
    @ApiResponse(responseCode = "400", description = "IP address marked as DENY")
    @ApiResponse(responseCode = "400", description = "IP address already marked as ALLOW")
    public HttpResponse<?> allowException(@Body CheckerBody args) {
        String ipAddress = args.getIpAddress();
        if (ipAddress == null) {
            return HttpResponse.badRequest("Missing argument: ipAddress");
        }

        try {
            SubnetUtils su = new SubnetUtils(ipAddress + "/32");
        } catch (Exception e) {
            return HttpResponse.badRequest("ipAddress is invalid");
        }

        FireholOverride override = fireholOverrideService.get(ipAddress);
        if (override != null) {
            if (override.getStatus().equals(FireholOverride.Status.DENY)) {
                return HttpResponse.badRequest(ipAddress + " is currently set to DENY");
            }
            return HttpResponse.badRequest(ipAddress + " already in exception list");
        }

        override = new FireholOverride();
        override.setIpAddress(ipAddress);
        override.setStatus(FireholOverride.Status.ALLOW);
        fireholOverrideService.save(override);
        return HttpResponse.created(ipAddress + " set to ALLOW");
    }

    @Post(value = "/exception/delete", consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
    @Operation(summary = "Delete exception for a specific IP address",
            description = "Delete an exception for a specific IP address")
    @Parameter(name = "ipAddress", description = "The IP address to search for")
    @ApiResponse(responseCode = "200", description = "delete succesful")
    @ApiResponse(responseCode = "400", description = "ipAddress missing from body")
    @ApiResponse(responseCode = "400", description = "Invalid IP address provided")
    @ApiResponse(responseCode = "400", description = "No exceptions for this IP address exist")
    public HttpResponse<?> deleteException(@Body CheckerBody args) {
        String ipAddress = args.getIpAddress();
        if (ipAddress == null) {
            return HttpResponse.badRequest("Missing argument: ipAddress");
        }

        try {
            SubnetUtils su = new SubnetUtils(ipAddress + "/32");
        } catch (Exception e) {
            return HttpResponse.badRequest("ipAddress is invalid");
        }

        FireholOverride override = fireholOverrideService.get(ipAddress);
        if (override != null) {
            fireholOverrideService.delete(ipAddress);
            return HttpResponse.ok(ipAddress + " deleted");
        }
        return HttpResponse.badRequest(ipAddress + " not in exception list");
    }

    @Post(value = "/exception/deny", consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
    @Operation(summary = "Add a DENY exception for a specific IP address",
            description = "Add a DENY exception for a specific IP address, if it does not already exist")
    @ApiResponse(responseCode = "200", description = "DENY succesful")
    @ApiResponse(responseCode = "400", description = "ipAddress missing from body")
    @ApiResponse(responseCode = "400", description = "Invalid IP address provided")
    @ApiResponse(responseCode = "400", description = "IP address marked as ALLOW")
    @ApiResponse(responseCode = "400", description = "IP address already marked as DENY")
    public HttpResponse<?> denyException(@Body CheckerBody args) {
        String ipAddress = args.getIpAddress();
        if (ipAddress == null) {
            return HttpResponse.badRequest("Missing argument: ipAddress");
        }

        FireholOverride override = fireholOverrideService.get(ipAddress);
        if (override != null) {
            if (override.getStatus().equals(FireholOverride.Status.ALLOW)) {
                return HttpResponse.badRequest(ipAddress + " is currently set to ALLOW");
            }
            return HttpResponse.badRequest(ipAddress + " already in exception list");
        }

        override = new FireholOverride();
        override.setIpAddress(ipAddress);
        override.setStatus(FireholOverride.Status.DENY);
        fireholOverrideService.save(override);
        return HttpResponse.created(ipAddress + " set to DENY");
    }
}
